const ANCHORS: ReadonlyMap<string, [number, number]> = new Map([
      ["kokorowatari",      [50, 13]],
      ["statue_cerise",     [10, 20]],
      ["statue_orange",     [10, 20]],
      ["statue_pomme",      [10, 20]],
      ["statue_banane",     [10, 20]],
      ["statue_abricot",    [10, 20]],
      ["statue_litchi",     [10, 20]],
      ["statue_blob",       [10, 20]],
      ["statue_framboise",  [10, 20]],
      ["statue_georges",    [10, 20]],
]);

const EXTRACT_FRAMES: ReadonlyMap<string, FrameRef> = new Map([
    ["statue_citron",   ["]xUh6", 178]],
    ["statue_bombino",  ["]xUh6", 179]],
    ["statue_poire",    ["]xUh6", 180]],
    ["statue_fraise",   ["]xUh6", 181]],
    ["statue_kiwi",     ["]xUh6", 182]],
    ["statue_pasteque", ["]xUh6", 183]],
    ["statue_ananas",   ["]xUh6", 184]],
]);

const CLONE_SPRITES: ReadonlyMap<string, SpriteRef> = new Map([
]);

const EXTRA_FRAMES: ReadonlyMap<SpriteRef, ExtraFrameOptions> = new Map([
  [
    "[Of51(",
    {
      frames: [
        "hub"
      ],
    },
  ],
  [
    "]xUh6",
    {
      frames: [
        "kokorowatari",
        "statue_cerise",
        "statue_orange",
        "statue_pomme",
        "statue_banane",
        "statue_citron",
        "statue_bombino",
        "statue_poire",
        "statue_abricot",
        "statue_litchi",
        "statue_fraise",
        "statue_kiwi",
        "statue_pasteque",
        "statue_ananas",
        "statue_blob",
        "statue_framboise",
        "statue_georges",
      ],
    },
  ],

]);

////////////////////////////////////////////////////////////////////////////////

import { PlaceObject } from "swf-types/tags";
import { getGamePath } from "@eternalfest/game";
import fs from "fs";
import sysPath from "path";
import { emitSwf } from "swf-emitter";
import { DynMovie, swfMerge } from "swf-merge";
import { parseSwf } from "swf-parser";
import { CompressionMethod, Matrix, Movie, NamedId, Sfixed16P16, SpriteTag, Tag, TagType } from "swf-types";
import { DefineSprite, DefineSprite as DefineSpriteTag } from "swf-types/tags/define-sprite";
import { BlendMode } from "swf-types/blend-mode";
import { Sfixed8P8 } from "swf-types/fixed-point/sfixed8p8";

const PROJECT_ROOT: string = sysPath.join(__dirname, "..");

const INPUT_GAME: string = getGamePath();
const INPUT_ASSETS: string = sysPath.join(PROJECT_ROOT, "build", "assets.swf");
const OUTPUT_GAME: string = sysPath.join(PROJECT_ROOT, "build", "game.swf");

interface ExtraFrameOptions {
  readonly protectedDepths?: ReadonlySet<number>,
  readonly frames: ReadonlyArray<string>,
}

const PIXELS_PER_TWIP: number = 20;

type SpriteRef = string | number;

type FrameRef = [SpriteRef, number];

async function main() {
  const game: Movie = await readSwf(INPUT_GAME);
  let assets: Movie = await readSwf(INPUT_ASSETS);
  assets = applyAnchors(assets, ANCHORS);
  // await writeFile("assets.json", JSON.stringify(assets, null, 2) as any);
  let merged: Movie = swfMerge(game, assets);
  merged = extractFrames(merged, EXTRACT_FRAMES);
  merged = cloneSprites(merged, CLONE_SPRITES);
  merged = appendFrames(merged, EXTRA_FRAMES);
  // await writeFile("game.json", JSON.stringify(merged, null, 2) as any);
  await writeSwf(OUTPUT_GAME, merged);
}

const IDENTITY_MATRIX: Matrix = {
  scaleX: Sfixed16P16.fromValue(1),
  scaleY: Sfixed16P16.fromValue(1),
  rotateSkew0: Sfixed16P16.fromValue(0),
  rotateSkew1: Sfixed16P16.fromValue(0),
  translateX: 0,
  translateY: 0,
};

function applyAnchors(astMovie: Movie, anchors: ReadonlyMap<string, [number, number]>): Movie {
  const movie = DynMovie.fromAst(astMovie);
  const exports = movie.getNamedExports();
  const definitions = movie.getDefinitions();
  const tags: Tag[] = [...astMovie.tags];
  for (const [linkageName, [posX, posY]] of anchors) {
    const id = exports.get(linkageName);
    if (id === undefined) {
      throw new Error(`ExportNotFound: linkageName = ${JSON.stringify(linkageName)}`);
    }
    const index = definitions.get(id);
    if (index === undefined) {
      throw new Error(`DefinitionNotFound: id = ${id}`);
    }
    const tag: Tag = tags[index];
    if (tag.type !== TagType.DefineSprite) {
      throw new Error(`UnexpectTagType: expected = ${TagType.DefineSprite} (DefineSprite), actual = ${tag.type} (${TagType[tag.type]})`);
    }
    const childTags: ReadonlyArray<SpriteTag> = tag.tags.map((childTag: SpriteTag) => {
      if (childTag.type !== TagType.PlaceObject || childTag.isUpdate) {
        return childTag;
      }
      const oldMatrix: Matrix = childTag.matrix !== undefined ? childTag.matrix : IDENTITY_MATRIX;
      const matrix = {
        ...oldMatrix,
        translateX: oldMatrix.translateX - (posX * PIXELS_PER_TWIP),
        translateY: oldMatrix.translateY - (posY * PIXELS_PER_TWIP),
      };
      return {...childTag, matrix};
    });
    tags[index] = {...tag, tags: childTags};
  }
  return {...astMovie, tags};
}

function cloneSprites(astMovie: Movie, sprites: ReadonlyMap<string, SpriteRef>): Movie {
  const movie = DynMovie.fromAst(astMovie);
  let nextId: number = movie.getMaxCharacterId() + 1;
  const tags: Tag[] = [...astMovie.tags];
  const extraExports: NamedId[] = [];
  for (const [cloneLinkName, spriteRef] of sprites) {
    const {index, sprite} = resolveSpriteRef(tags, movie, spriteRef);
    const clone: DefineSprite = {...sprite, id: nextId};
    tags.push(clone);
    extraExports.push({id: clone.id, name: cloneLinkName});
    nextId += 1;
  }
  tags.push({type: TagType.ExportAssets, assets: extraExports});
  return {...astMovie, tags};
}

function extractFrames(astMovie: Movie, anchors: ReadonlyMap<string, FrameRef>): Movie {
  const movie = DynMovie.fromAst(astMovie);
  let nextId: number = movie.getMaxCharacterId() + 1;
  const tags: Tag[] = [...astMovie.tags];
  const extraExports: NamedId[] = [];
  for (const [linkName, [spriteRef, frameIndex]] of anchors) {
    const {index, sprite} = resolveSpriteRef(tags, movie, spriteRef);
    const frameSprite: DefineSprite = extractFrame(sprite, frameIndex, nextId);
    tags.push(frameSprite);
    extraExports.push({id: frameSprite.id, name: linkName});
    nextId += 1;
  }
  tags.push({type: TagType.ExportAssets, assets: extraExports});
  return {...astMovie, tags};
  function extractFrame(sprite: DefineSprite, frameIndex: number, id: number): DefineSprite {
    let curFrame: number = 0;
    let curTagIndex: number = 0;
    const depths: Map<number, PlaceObject> = new Map();
    for (const tag of sprite.tags) {
      if (curFrame === frameIndex) {
        // We reached the frame we wanted to extract
        const spriteTags: SpriteTag[] = [];
        for (const object of depths.values()) {
          spriteTags.push(object);
        }
        for (let i: number = curTagIndex; i < sprite.tags.length; i++) {
          const spriteTag: SpriteTag = sprite.tags[i];
          if (spriteTag.type === TagType.ShowFrame) {
            break;
          }
          spriteTags.push(spriteTag);
        }
        spriteTags.push({type: TagType.ShowFrame});
        return {type: TagType.DefineSprite, id, frameCount: 1, tags: spriteTags};
      }

      if (tag.type === TagType.ShowFrame) {
        curFrame += 1;
      } else if (tag.type === TagType.PlaceObject) {
        if (!tag.isUpdate) {
          depths.set(tag.depth, tag);
        } else {
          let old: PlaceObject | undefined = depths.get(tag.depth);
          if (old === undefined) {
            old = {
              type: TagType.PlaceObject,
              isUpdate: false,
              depth: tag.depth,
              characterId: tag.characterId,
              className: tag.className,
              matrix: {
                scaleX: Sfixed16P16.fromValue(1),
                scaleY: Sfixed16P16.fromValue(1),
                rotateSkew0: Sfixed16P16.fromValue(0),
                rotateSkew1: Sfixed16P16.fromValue(0),
                translateX: 0,
                translateY: 0,
              },
              colorTransform: {
                redMult: Sfixed8P8.fromValue(1),
                greenMult: Sfixed8P8.fromValue(1),
                blueMult: Sfixed8P8.fromValue(1),
                alphaMult: Sfixed8P8.fromValue(1),
                redAdd: 0,
                greenAdd: 0,
                blueAdd: 0,
                alphaAdd: 0,
              },
              ratio: 0,
              name: tag.name,
              clipDepth: tag.clipDepth,
              filters: undefined,
              blendMode: BlendMode.Normal,
              bitmapCache: undefined,
              visible: undefined,
              backgroundColor: undefined,
              clipActions: undefined,
            };
          }
          const newTag: PlaceObject = {
            ...old,
            isUpdate: false,
            characterId: tag.characterId !== undefined ? tag.characterId : old.characterId,
            className: tag.className !== undefined ? tag.className : old.className,
            matrix: tag.matrix !== undefined ? tag.matrix : old.matrix,
            colorTransform: tag.colorTransform !== undefined ? tag.colorTransform : old.colorTransform,
            ratio: tag.ratio !== undefined ? tag.ratio : old.ratio,
            name: tag.name !== undefined ? tag.name : old.name,
            clipDepth: tag.clipDepth !== undefined ? tag.clipDepth : old.clipDepth,
            filters: tag.filters !== undefined ? tag.filters : old.filters,
            blendMode: tag.blendMode !== undefined ? tag.blendMode : old.blendMode,
            bitmapCache: tag.bitmapCache !== undefined ? tag.bitmapCache : old.bitmapCache,
            visible: tag.visible !== undefined ? tag.bitmapCache : old.visible,
            backgroundColor: tag.backgroundColor !== undefined ? tag.backgroundColor : old.backgroundColor,
            clipActions: tag.clipActions !== undefined ? tag.clipActions : old.clipActions,
          };
          depths.set(tag.depth, newTag);
        }
      } else if (tag.type === TagType.RemoveObject) {
        depths.delete(tag.depth);
      }

      curTagIndex += 1;
    }
    throw new Error("FrameNotFound: " + frameIndex);
  }
}

function appendFrames(astMovie: Movie, extraFrames: ReadonlyMap<SpriteRef, ExtraFrameOptions>) {
  const movie = DynMovie.fromAst(astMovie);
  const exports = movie.getNamedExports();
  const tags: Tag[] = [...astMovie.tags];
  for (const [extendedLinkageName, options] of extraFrames) {
    const protectedDepths: ReadonlySet<number> = options.protectedDepths !== undefined
      ? options.protectedDepths
      : new Set();
    const {index, sprite} = resolveSpriteRef(tags, movie, extendedLinkageName);
    tags[index] = appendFramesToSprite(sprite, options.frames, extendedLinkageName, protectedDepths);
  }
  return {...astMovie, tags};

  function appendFramesToSprite(
    tag: DefineSpriteTag,
    frames: ReadonlyArray<string>,
    extendedLinkageName: SpriteRef,
    protectedDepths: ReadonlySet<number>,
  ): DefineSpriteTag {
    let frameCount: number = tag.frameCount;
    const childTags: SpriteTag[] = [...tag.tags];
    for (const depth of getUsedDepths(childTags)) {
      if (protectedDepths.has(depth)) {
        continue;
      }
      childTags.push({type: TagType.RemoveObject, depth});
    }
    let minDepth = 0;
    for (const depth of protectedDepths) {
      minDepth = Math.max(minDepth, depth);
    }
    const depth = minDepth + 1;
    let first = true;
    for (const frameLinkageName of frames) {
      const id = exports.get(frameLinkageName);
      if (id === undefined) {
        throw new Error(`ExportNotFound: linkageName = ${JSON.stringify(frameLinkageName)}`);
      }
      console.log(`${extendedLinkageName}[${frameCount}] = ${frameLinkageName}`);
      if (first) {
        first = false;
      } else {
        childTags.push({type: TagType.RemoveObject, depth});
      }
      childTags.push(
        {
          type: TagType.PlaceObject,
          isUpdate: false,
          depth,
          characterId: id,
          className: undefined,
          matrix: undefined,
          colorTransform: undefined,
          ratio: undefined,
          name: undefined,
          clipDepth: undefined,
          filters: undefined,
          blendMode: undefined,
          bitmapCache: undefined,
          visible: undefined,
          backgroundColor: undefined,
          clipActions: undefined,
        },
        {type: TagType.ShowFrame},
      );
      frameCount += 1;
    }

    return {...tag, frameCount, tags: childTags};
  }
}

function getUsedDepths(tags: Iterable<Tag>): Set<number> {
  const depths: Set<number> = new Set();
  for (const tag of tags) {
    if (tag.type === TagType.PlaceObject) {
      depths.add(tag.depth);
    } else if (tag.type === TagType.RemoveObject) {
      depths.delete(tag.depth);
    }
  }
  return depths;
}

function resolveSpriteRef(tags: readonly Tag[], movie: DynMovie, ref: SpriteRef): {index: number, sprite: DefineSprite} {
  const exports = movie.getNamedExports();
  const definitions = movie.getDefinitions();
  let resolvedId: number
  if (typeof ref === "number") {
    resolvedId = ref;
  } else {
    const refParts: readonly string[] = typeof ref === "string" ? ref.split("/") : [];
    const spriteName: string = refParts[0];
    const objectName: string | undefined = 1 < refParts.length ? refParts[1] : undefined;
    const spriteId: number | undefined = exports.get(spriteName);
    if (spriteId === undefined) {
      throw new Error(`ExportNotFound: linkageName = ${JSON.stringify(spriteName)}`);
    }
    if (objectName === undefined) {
      resolvedId = spriteId;
    } else {
      const spriteIndex = definitions.get(spriteId);
      if (spriteIndex === undefined) {
        throw new Error(`DefinitionNotFound: id = ${spriteId}`);
      }
      const tag: Tag = tags[spriteIndex];
      if (tag.type !== TagType.DefineSprite) {
        throw new Error(`UnexpectTagType: expected = ${TagType.DefineSprite} (DefineSprite), actual = ${tag.type} (${TagType[tag.type]})`);
      }
      let subSpriteId: number | undefined = undefined;
      for (const childTag of tag.tags) {
        if (childTag.type === TagType.PlaceObject && childTag.name === objectName) {
          subSpriteId = childTag.characterId;
        }
      }
      if (subSpriteId === undefined) {
        throw new Error(`CharacterIdNotFound: extendedLinkageName = ${JSON.stringify(ref)}`);
      }
      resolvedId = subSpriteId;
    }
  }
  const index: number | undefined = definitions.get(resolvedId);
  if (index === undefined) {
    throw new Error(`DefinitionNotFound: id = ${resolvedId}`);
  }
  const spriteTag: Tag = tags[index];
  if (spriteTag.type !== TagType.DefineSprite) {
    throw new Error(`UnexpectTagType: expected = ${TagType.DefineSprite} (DefineSprite), actual = ${spriteTag.type} (${TagType[spriteTag.type]})`);
  }
  return {index, sprite: spriteTag};
}

async function readSwf(filePath: string): Promise<Movie> {
  const buffer: Buffer = await readFile(filePath);
  return parseSwf(buffer);
}

async function readFile(filePath: string): Promise<Buffer> {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, (err, data) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

async function writeSwf(filePath: string, movie: Movie): Promise<void> {
  const bytes: Uint8Array = emitSwf(movie, CompressionMethod.Deflate);
  return writeFile(filePath, bytes);
}

async function writeFile(filePath: string, data: Uint8Array): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    fs.writeFile(filePath, data, (err: NodeJS.ErrnoException | null) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

main();

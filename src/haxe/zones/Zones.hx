package zones;

import patchman.DebugConsole;
import etwin.Obfu;
import etwin.ds.FrozenArray;
import etwin.ds.WeakMap;
import patchman.IPatch;
import patchman.Ref;

import atlas.PropBridge;
import atlas.Property;
import atlas.PropTypes;
import atlas.Atlas;

@:build(patchman.Build.di())
class Zones {
    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    public static var NOREWARD_KEY(default, null): Property<Bool> = new Property();

    @:diExport
    public var noRewardProperty(default, null): PropBridge = NOREWARD_KEY.bridge(Obfu.raw("noReward"), PropTypes.BOOL);

    public function new() {
        var patches = [
            Ref.auto(hf.entity.Bad.dropReward).wrap(function(hf: hf.Hf, self: hf.entity.Bad, old) {
                var noReward: Bool = Atlas.getProperty(self.game, NOREWARD_KEY).or(_ => false);
                if (noReward)
                    return null;
                old(self);
                return;
            }),
        ];

        this.patches = FrozenArray.from(patches);
    }
}

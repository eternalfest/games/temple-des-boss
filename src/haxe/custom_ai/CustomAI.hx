package custom_ai;

import merlin.IAction;
import patchman.IPatch;
import patchman.Ref;
import etwin.ds.FrozenArray;
import etwin.ds.WeakMap;
import keyboard.Key;
import keyboard.KeyCode;


@:build(patchman.Build.di())
class CustomAI {
    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;


    public function new(): Void {
        var patches = [
            Ref.auto(hf.entity.bad.Jumper.onFall).wrap(function(hf: hf.Hf, self: hf.entity.bad.Jumper, old): Void {

                if (self.fl_jumper) {
                    if (self.isReady() && self.fl_jH) {
                        if (self.fl_fall && self.decideFall()) {
                            if (self.world.checkFlag({x: self.cx, y: self.cy}, hf.Data.IA_ALLOW_FALL)) {
                                self.fl_willFallDown = true;
                            }
                        }
                        if (self.fl_fall && self.fl_climb) {
                            if (self.world.checkFlag({x: self.cx, y: self.cy + 1}, hf.Data.IA_CLIMB_LEFT) || self.world.checkFlag({x: self.cx, y: self.cy + 1}, hf.Data.IA_CLIMB_RIGHT)) {
                                self.fl_willFallDown = true;
                            }
                        }
                        if (!self.fl_willFallDown) {
                            if ((cast self).peutSauter3) {
                                if (self.dx < 0 && self.world.checkFlag({"x": self.cx, "y": self.cy}, (cast hf.Data).IA_JUMP_LEFT3)) {
                                    self.jump(-6.6, -10.2, 0);
                                    self.adjustToRight();
                                }
                                if (self.dx > 0 && self.world.checkFlag({"x": self.cx, "y": self.cy}, (cast hf.Data).IA_JUMP_RIGHT3)) {
                                    self.jump(6.6, -10.2, 0);
                                    self.adjustToLeft();
                                }
                            }
                            if (self.dx < 0 && self.world.checkFlag({x: self.cx, y: self.cy}, hf.Data.IA_JUMP_LEFT)) {
                                self.jump(-hf.Data.BAD_HJUMP_X, -hf.Data.BAD_HJUMP_Y, 0);
                                self.adjustToRight();
                            }
                            if (self.dx > 0 && self.world.checkFlag({x: self.cx, y: self.cy}, hf.Data.IA_JUMP_RIGHT)) {
                                self.jump(hf.Data.BAD_HJUMP_X, -hf.Data.BAD_HJUMP_Y, 0);
                                self.adjustToLeft();
                            }
                            if (self.fl_climb) {
                                self.checkClimb();
                            }
                        }
                    }
                }
                Ref.call(hf, super.onFall, self);
            }),
            Ref.auto(hf.entity.bad.Jumper.infix).before(function(hf: hf.Hf, self: hf.entity.bad.Jumper): Void {
                if (self.fl_jumper) {
                    self.updateCoords();
                    if ((cast self).peutMonter3) {
                        if (self.decideJumpUp()) {
                            if (self.world.checkFlag({"x": self.cx, "y": self.cy}, (cast hf.Data).IA_JUMP_UP3)) {
                                self.jump(0, -22, hf.Data.SECOND);
                                self.fl_stopStepping = true;
                            }
                        }
                    }
                }
            }),
            Ref.auto(hf.levels.GameMechanics.parseCurrentIA).before(function(hf: hf.Hf, self: hf.levels.GameMechanics, it: hf.Case): Void {
                hf.Data.BAD_VJUMP_Y_LIST = [];
                for (i in 0...24)
                    hf.Data.BAD_VJUMP_Y_LIST.push(11 + 3.5 * i);
                hf.Data.IA_CLIMB = 24;

                if ((cast hf.Data).IA_JUMP_LEFT3 != 8192) {
                    hf.Data.GRID_NAMES.push("$IA_TILE_JUMP_LEFT_3");
                    (cast hf.Data).IA_JUMP_LEFT3 = 8192;
                }
                if ((cast hf.Data).IA_JUMP_RIGHT3 != 16384) {
                    hf.Data.GRID_NAMES.push("$IA_TILE_JUMP_RIGHT_3");
                    (cast hf.Data).IA_JUMP_RIGHT3 = 16384;
                }
                if ((cast hf.Data).IA_JUMP_UP3 != 32768) {
                    hf.Data.GRID_NAMES.push("$IA_TILE_JUMP_UP_3");
                    (cast hf.Data).IA_JUMP_UP3 = 32768;
                }
            }),
            Ref.auto(hf.levels.GameMechanics.parseCurrentIA).wrap(function(hf: hf.Hf, self: hf.levels.GameMechanics, it: hf.Case, old): Void {
                var v3 = 0;
                var v4 = hf.Data.LEVEL_WIDTH * hf.Data.LEVEL_HEIGHT;
                var v5 = it.cx;
                var v6 = it.cy;
                while (true) {
                    if (!(v3 < hf.Data.MAX_ITERATION && v6 < hf.Data.LEVEL_HEIGHT)) break;
                    var v7 = 0;
                    if (self.getCase({x: v5, y: v6 + 1}) == hf.Data.GROUND) {
                        v7 |= hf.Data.IA_TILE_TOP;
                        if (self.getCase({x: v5, y: v6 - hf.Data.IA_VJUMP}) == hf.Data.GROUND && self.getCase({x: v5, y: v6 - hf.Data.IA_VJUMP - 1}) <= 0) {
                            v7 |= hf.Data.IA_JUMP_UP;
                        }
                        if (self.getCase({x: v5, y: v6 - 3}) == hf.Data.GROUND && self.getCase({x: v5, y: v6 - 4}) <= 0) {
                            v7 |= (cast hf.Data).IA_JUMP_UP3;
                        }
                    }

                    if (self.getCase({x: v5, y: v6}) == hf.Data.GROUND && self.getCase({x: v5, y: v6 - 1}) <= 0) {
                        v7 |= hf.Data.IA_TILE;
                    }

                    var v8 = self._checkSecureFall(v5, v6);
                    self.fallMap[v5][v6] = v8;
                    if (v8 >= 0) {
                        v7 |= hf.Data.IA_ALLOW_FALL;
                    }
                    if (self.getCase({x: v5, y: v6}) == 0 && self.getCase({x: v5, y: v6 + 1}) == hf.Data.GROUND) {
                        v8 = self._checkSecureFall(v5, v6 + 2);
                        if (v8 >= 0) {
                            v7 |= hf.Data.IA_JUMP_DOWN;
                        }
                    }
                    if (self.getCase({x: v5, y: v6 + 1}) == hf.Data.GROUND && (self.getCase({x: v5 - 1, y: v6 + 1}) <= 0 || self.getCase({x: v5 + 1, y: v6 + 1}) <= 0)) {
                        v7 |= hf.Data.IA_BORDER;
                    }
                    if (self.getCase({x: v5, y: v6 + 1}) <= 0 && (self.getCase({x: v5 - 1, y: v6 + 1}) == hf.Data.GROUND || self.getCase({x: v5 + 1, y: v6 + 1}) == hf.Data.GROUND)) {
                        v7 |= hf.Data.IA_FALL_SPOT;
                    }
                    if ((v7 & hf.Data.IA_BORDER) > 0) {
                        if (self.getCase({x: v5 - 1, y: v6 + 1}) != hf.Data.GROUND && self.getCase({x: v5 + 1, y: v6 + 1}) != hf.Data.GROUND) {
                            v7 |= hf.Data.IA_SMALL_SPOT;
                        }
                    }
                    if ((v7 & hf.Data.IA_TILE_TOP) > 0) {
                        var v9 = 1;
                        var v10 = 1;
                        while (v10 <= 5) {
                            if (self.getCase({x: v5, y: v6 - v10}) <= 0) {
                                ++v9;
                            } else {
                                v10 = 999;
                            }
                            ++v10;
                        }
                        if (v9 > 0) {
                            if (self.getCase({x: v5 - 1, y: v6}) > 0) {
                                var v11 = self.getWallHeight(v5 - 1, v6, hf.Data.IA_CLIMB);
                                if (v11 != null && v11 < v9 && v6 - v11 >= 0) {
                                    v7 |= hf.Data.IA_CLIMB_LEFT;
                                }
                            }
                            if (self.getCase({x: v5 + 1, y: v6}) > 0) {
                                var v12 = self.getWallHeight(v5 + 1, v6, hf.Data.IA_CLIMB);
                                if (v12 != null && v12 < v9 && v6 - v12 >= 0) {
                                    v7 |= hf.Data.IA_CLIMB_RIGHT;
                                }
                            }
                        }
                    }
                    if ((v7 & hf.Data.IA_FALL_SPOT) > 0) {
                        var v13 = 1;
                        var v14 = 1;
                        while (v14 <= 5) {
                            if (self.getCase({x: v5, y: v6 - v14}) <= 0) {
                                ++v13;
                            } else {
                                v13 += 2;
                                v14 = 999;
                            }
                            ++v14;
                        }
                        if (v13 > 0) {
                            if (self.getCase({x: v5 + 1, y: v6 + 1}) == hf.Data.GROUND) {
                                var v15 = self.getStepHeight(v5, v6, hf.Data.IA_CLIMB);
                                if (v15 != null && v15 < v13) {
                                    if (self.checkFlag({x: v5, y: v6 - v15}, hf.Data.IA_BORDER) && self.checkFlag({x: v5 + 1, y: v6 - v15}, hf.Data.IA_FALL_SPOT)) {
                                        v7 |= hf.Data.IA_CLIMB_LEFT;
                                    }
                                }
                            }
                            if (self.getCase({x: v5 - 1, y: v6 + 1}) == hf.Data.GROUND) {
                                var v16 = self.getStepHeight(v5, v6, hf.Data.IA_CLIMB);
                                if (v16 != null && v16 < v13) {
                                    if (self.checkFlag({x: v5, y: v6 - v16}, hf.Data.IA_BORDER) && self.checkFlag({x: v5 - 1, y: v6 - v16}, hf.Data.IA_FALL_SPOT)) {
                                        v7 |= hf.Data.IA_CLIMB_RIGHT;
                                    }
                                }
                            }
                        }
                    }
                    if ((v7 & hf.Data.IA_FALL_SPOT) > 0) {
                        if (self.getCase({x: v5 + 1, y: v6 + 1}) == hf.Data.GROUND && self.getCase({x: v5 - hf.Data.IA_HJUMP, y: v6 + 1}) == hf.Data.GROUND) {
                            if (self.getCase({x: v5 - 1, y: v6}) <= 0) {
                                v7 |= hf.Data.IA_JUMP_LEFT;
                            }
                        }
                        if (self.getCase({x: v5 - 1, y: v6 + 1}) == hf.Data.GROUND && self.getCase({x: v5 + hf.Data.IA_HJUMP, y: v6 + 1}) == hf.Data.GROUND) {
                            if (self.getCase({x: v5 + 1, y: v6}) <= 0) {
                                v7 |= hf.Data.IA_JUMP_RIGHT;
                            }
                        }
                        if (self.getCase({x: v5 + 1, y: v6 + 1}) == hf.Data.GROUND && self.getCase({x: v5 - 3, y: v6 + 1}) == hf.Data.GROUND && self.getCase({x: v5 - 2, y: v6 + 1}) <= 0) {
                            if (self.getCase({x: v5 - 1, y: v6}) <= 0 && self.getCase({x: v5 - 2, y: v6}) <= 0) {
                                v7 |= (cast hf.Data).IA_JUMP_LEFT3;
                            }
                        }
                        if (self.getCase({x: v5 - 1, y: v6 + 1}) == hf.Data.GROUND && self.getCase({x: v5 + 3, y: v6 + 1}) == hf.Data.GROUND && self.getCase({x: v5 + 2, y: v6 + 1}) <= 0) {
                            if (self.getCase({x: v5 + 1, y: v6}) <= 0 && self.getCase({x: v5 + 2, y: v6}) <= 0) {
                                v7 |= (cast hf.Data).IA_JUMP_RIGHT3;
                            }
                        }
                    }
                    self.flagMap[v5][v6] = v7;
                    ++v5;
                    if (v5 >= hf.Data.LEVEL_WIDTH) {
                        v5 = 0;
                        ++v6;
                    }
                    ++v3;
                }
                self.manager.progress(v6 / hf.Data.LEVEL_HEIGHT);
                if (v3 != hf.Data.MAX_ITERATION) {
                    self.onParseIAComplete();
                }
                it.cx = v5;
                it.cy = v6;
            }),

        ];
        this.patches = FrozenArray.from(patches);
    }

}

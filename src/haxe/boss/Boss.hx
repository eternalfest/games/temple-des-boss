package boss;

import boss.actions.BossBad;
import boss.actions.InitFruitPipes;
import boss.actions.AddFruitPipe;
import boss.actions.BossHPBelow;
import boss.actions.CheckBoss;
import boss.actions.AddBossLever;

import merlin.IAction;
import patchman.IPatch;
import patchman.Ref;
import etwin.ds.FrozenArray;
import etwin.ds.WeakMap;
import keyboard.Key;
import keyboard.KeyCode;


@:build(patchman.Build.di())
class Boss {
    @:diExport
    public var actions(default, null): FrozenArray<IAction>;
    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    public var boss: Dynamic;
    public var bossActive: Bool = false;
    public static var IS_BOSS_FRUIT(default, never): WeakMap<hf.entity.Bad, Bool> = new WeakMap();

    public var fruitPipes: Dynamic;
    public static var FRUIT_PIPE_SPAWN_TIMER(default, never): WeakMap<hf.entity.Bad, Int> = new WeakMap();
    public static var FRUIT_PIPE_DIR(default, never): WeakMap<hf.entity.Bad, Int> = new WeakMap();

    public var levers: Dynamic;

    public function new(): Void {
        this.actions = FrozenArray.of(
            new BossBad(this),
            new InitFruitPipes(this),
            new AddFruitPipe(this),
            new BossHPBelow(this),
            new CheckBoss(this),
            new AddBossLever(this)
        );

        var patches = [
            Ref.auto(hf.entity.bomb.player.Black.onExplode).wrap(function(hf: hf.Hf, self: hf.entity.bomb.player.Black, old): Void {
                Ref.call(hf, super.onExplode, self);
                self.game.shake(10, 4);
                self.game.fxMan.attachExplodeZone(self.x, self.y, self.radius);

                var closeEnemies: Array<hf.entity.Bad> = cast self.bombGetClose(self.game.root.Data.BAD);
                for (closeEnemy in closeEnemies) {
                    if (IS_BOSS_FRUIT.get(closeEnemy)) {
                        if (!this.boss.fl_knock && this.boss.canBeHitByBlackBomb) {
                            this.boss.loseHP(self.game, 10);
                            this.boss.forceKnock(self.game.root.Data.SECOND * (this.boss.id != self.game.root.Data.BAD_ANANAS ? 1.7 : 3.3));
                        }
                    }
                    else {
                        closeEnemy.setCombo(self.uniqId);
                        closeEnemy.killHit(0);
                        self.shockWave(closeEnemy, self.radius, self.power);
                        closeEnemy.dy = -10 - Std.random(20);
                    }
                }
            }),
            Ref.auto(hf.entity.bad.walker.Bombe.selfDestruct).before(function(hf: hf.Hf, self: hf.entity.bad.walker.Bombe): Void {
                if (self.distance(this.boss.x, this.boss.y) <= hf.entity.bad.walker.Bombe.RADIUS) {
                    if (this.boss.bombinoExplosionTimer == 0) {
                        this.boss.bombinoExplosionTimer = self.game.root.Data.SECOND * 1.5;
                        this.boss.fl_blinking = true;
                    }
                }
            }),
            Ref.auto(hf.entity.bomb.bad.Mine.onExplode).before(function(hf: hf.Hf, self: hf.entity.bomb.bad.Mine): Void {
                if (!self.fl_trigger) {
                    return;
                }

                for (lever in (this.levers.list : Array<Dynamic>)) {
                    if (lever.active == false && self.distance(lever.x * 20 + 10, lever.y * 20 + 10) <= self.radius) {
                        lever.active = true;
                        lever.sprite.removeMovieClip();
                        lever.sprite = self.game.world.view.attachSprite("$levier2", lever.x * 20, (lever.y + 1) * 20, true);
                    }
                }
                this.checkIfAllLeversActivated(self.game);
            }),
            Ref.auto(hf.entity.bad.ww.Crawler.attack).wrap(function(hf: hf.Hf, self: hf.entity.bad.ww.Crawler, old): Void {
                if (!IS_BOSS_FRUIT.get(self)) {
                    old(self);
                    return;
                }

                if (Std.random(cast (this.boss.hp / 10)) != 0) {
                    var tir = hf.entity.shoot.FireBall.attach(self.game, self.x, self.y);
                    tir.moveTo(self.x, self.y);
                    tir.dx = -self.cp.x * hf.entity.bad.ww.Crawler.SHOOT_SPEED;
                    tir.dy = -self.cp.y * hf.entity.bad.ww.Crawler.SHOOT_SPEED;
                    tir.scale(140);
                }
                else {
                    for (i in 0...8) {
                        var tir = hf.entity.shoot.FireBall.attach(self.game, self.x, self.y);
                        tir.moveToAng(45 * i, 4);
                    }
                }
                var v3 = Std.random(3) + 2;
                if (self.cp.x != 0) {
                    self.game.fxMan.inGameParticlesDir(self.game.root.Data.PARTICLE_BLOB, self.x, self.y, v3, -self.cp.x);
                } else {
                    self.game.fxMan.inGameParticles(self.game.root.Data.PARTICLE_BLOB, self.x, self.y, v3);
                }
                self.game.fxMan.attachExplosion(self.x, self.y, 20);
                self.sub._xscale = 150 + Math.abs(self.cp.x) * 150;
                self.sub._yscale = 150 + Math.abs(self.cp.y) * 150;
                self.colorAlpha = hf.entity.bad.ww.Crawler.COLOR_ALPHA;
                self.setColorHex(Math.round(self.colorAlpha), hf.entity.bad.ww.Crawler.COLOR);
                self.attackCD = hf.entity.bad.ww.Crawler.COOLDOWN;
                self.playAnim(self.game.root.Data.ANIM_BAD_SHOOT_END);
            }),
            Ref.auto(hf.entity.bad.walker.Ananas.attack).wrap(function(hf: hf.Hf, self: hf.entity.bad.walker.Ananas, old): Void {
                if (!IS_BOSS_FRUIT.get(self)) {
                    old(self);
                    return;
                }

                var v2 = self.game.fxMan.attachExplodeZone(self.x, self.y, self.dashRadius);
                v2.mc._alpha = 20;
                self.game.shake(hf.Data.SECOND * 0.5, 5);
                for (player in self.game.getPlayerList()) {
                    if (player.fl_stable) {
                        if (player.fl_shield) {
                            player.dy = -8 * 2;
                        } else {
                            player.knock(hf.Data.PLAYER_KNOCK_DURATION * 2);
                        }
                    }
                }
                self.repel(hf.Data.BOMB, 1);
                self.repel(hf.Data.PLAYER, 2);
                self.vaporize(hf.Data.PLAYER_SHOOT);
                self.fl_attack = false;
                self.unstick();
            }),
            Ref.auto(hf.entity.bad.walker.Pomme.onShoot).wrap(function(hf: hf.Hf, self: hf.entity.bad.walker.Pomme, old): Void {
                if (!IS_BOSS_FRUIT.get(self)) {
                    old(self);
                    return;
                }

                if (Std.random(2) == 0 && this.boss.pommeAttackCount == 0) {
                    var pepin = hf.entity.shoot.Pepin.attach(self.game, self.x, self.y);
                    pepin._xscale = 200;
                    pepin._yscale = 200;
                    if (self.dir < 0) {
                        pepin.moveLeft(pepin.shootSpeed * 1.5);
                    } else {
                        pepin.moveRight(pepin.shootSpeed * 1.5);
                    }
                }
                else {
                    if (this.boss.pommeAttackCount == 0) {
                        this.boss.pommeAttackCount = 3;
                    }
                    if (this.boss.pommeAttackCount > 0) {
                        this.boss.pommeAttackCount--;

                        var pepin = hf.entity.shoot.Pepin.attach(self.game, self.x, self.y);
                        pepin.x += 30 * self.dir;
                        pepin.y -= 10;
                        if (self.dir < 0) {
                            pepin.moveLeft(pepin.shootSpeed);
                        } else {
                            pepin.moveRight(pepin.shootSpeed);
                        }

                        if (this.boss.pommeAttackCount > 0) {
                            var old = this.boss.shootPreparation;
                            this.boss.shootPreparation = 10;
                            this.boss.startShoot();
                            this.boss.shootPreparation = old;
                        }
                    }
                }
            }),
            Ref.auto(hf.entity.bad.walker.Citron.onShoot).wrap(function(hf: hf.Hf, self: hf.entity.bad.walker.Citron, old): Void {
                if (!IS_BOSS_FRUIT.get(self)) {
                    old(self);
                    return;
                }

                if (Std.random(2) == 0) {
                    var pepin = hf.entity.shoot.Zeste.attach(self.game, self.x, self.y);
                    pepin._xscale = 200;
                    pepin._yscale = 200;
                    pepin.moveUp(pepin.shootSpeed * 1.5);
                }
                else {
                    var pepin = hf.entity.shoot.Zeste.attach(self.game, self.x, self.y - 30);
                    pepin.moveUp(6);
                    this.boss.citronAttackTimer = 150;
                    this.boss.fl_shooter = false;
                    for (i in 0...5) {
                        this.boss.angerMore();
                    }
                }
            }),
            Ref.auto(hf.entity.bad.walker.Poire.onShoot).wrap(function(hf: hf.Hf, self: hf.entity.bad.walker.Poire, old): Void {
                if (!IS_BOSS_FRUIT.get(self)) {
                    old(self);
                    return;
                }

                for (i in 0...Std.random(3) + 1) {
                    var bomb = hf.entity.bomb.bad.PoireBomb.attach(self.game, self.x, self.y);
                    bomb.moveToAng((self.dir < 0 ? -135 : -45), 10 + 3 * i);
                }
                self.setNext(null, null, self.shootDuration, self.game.root.Data.ACTION_FALLBACK);
            }),
            Ref.auto(hf.entity.bad.walker.Kiwi.onShoot).wrap(function(hf: hf.Hf, self: hf.entity.bad.walker.Kiwi, old): Void {
                if (!IS_BOSS_FRUIT.get(self)) {
                    old(self);
                    return;
                }

                var mine = hf.entity.bomb.bad.Mine.attach(self.game, self.x + self.dir * self.game.root.Data.CASE_WIDTH * 1.1, self.y);
                mine.radius *= 1.5;
                mine._xscale *= 1.5;
                mine._yscale *= 1.5;
                self.mineList.push(mine);
            }),
            Ref.auto(hf.entity.bad.walker.Framboise.onShoot).wrap(function(hf: hf.Hf, self: hf.entity.bad.walker.Framboise, old): Void {
                if (!IS_BOSS_FRUIT.get(self)) {
                    old(self);
                    return;
                }

                if (self.y > 20 * self.game.root.Data.CASE_HEIGHT) {
                    self.tx = (Std.random(16) + 2) * 20;
                    self.ty = 4 * 20;
                }
                else {
                    self.tx = (Std.random(18) + 1) * 20;
                    self.ty = 24 * 20;
                }
                self.arrived = 0;
                self.phaseOut();
                for (i in 0...6) {
                    var grain = hf.entity.shoot.FramBall.attach(self.game, self.aroundX(self.x), self.aroundY(self.y));
                    grain.scale(150);
                    grain.setOwner(self);
                    grain.gotoAndStop('' + (i + 1));
                }
            }),
            Ref.auto(hf.entity.bad.FireBall.hit).before(function(hf: hf.Hf, self: hf.entity.bad.FireBall, e: hf.Entity): Void {
                if (!IS_BOSS_FRUIT.get(self)) {
                    return;
                }
                if ((e.types & self.game.root.Data.HU_BAD) > 0) {
                    this.boss.loseHP(self.game, 10);
                    self.game.fxMan.attachExplodeZone(e.x, e.y, 15);
                    self.game.fxMan.attachExplodeZone(e.x, e.y, 30);
                    e.destroy();
                }
            }),
            Ref.auto(hf.entity.Bad.hit).wrap(function(hf: hf.Hf, self: hf.entity.Bad, e: hf.Entity, old): Void {
                if (!IS_BOSS_FRUIT.get(self)) {
                    old(self, e);
                    return;
                }

                if (this.boss.dyingAnimationTimer > 0 || this.boss.fl_knock) {
                    return;
                }

                if ((e.types & self.game.root.Data.PLAYER_BOMB) > 0 && this.boss.eatsPlayerBombs) {
                    self.game.fxMan.attachExplodeZone(e.x, e.y, 30);
                    e.destroy();
                }

                if ((e.types & self.game.root.Data.BAD_CLEAR) > 0) {
                    var fruit: hf.entity.Bad = cast e;
                    if (fruit.fl_kill) {
                        return;
                    }
                    if (this.boss.canBeHitBySurfing && fruit.fl_freeze && (Math.abs(fruit.dx) > 5 || Math.abs(fruit.dy) > 5)) {
                        if (!(Std.is(fruit, hf.entity.bad.walker.Abricot._class()) && (cast fruit).fl_spawner)) {
                            this.boss.loseHP(self.game, 10);
                        }
                        fruit.killHit(0);
                    }
                    else if (this.boss.canKillFruits) {
                        if (Std.is(fruit, hf.entity.bad.walker.Litchi._class())) {
                            var eclair = self.game.depthMan.attach("hammer_fx_strike", self.game.root.Data.FX);
                            eclair._x = self.game.root.Data.GAME_WIDTH / 2;
                            eclair._y = fruit._y;
                            self.game.fxMan.attachExplodeZone(fruit.x, fruit.y, 15);
                            fruit.destroy();
                        }
                        fruit.killHit(0);
                    }
                }

                if ((e.types & self.game.root.Data.PLAYER) > 0) {
                    var player: hf.entity.Player = cast e;
                    if (!player.fl_kill) {
                        player.killHit(0);
                    }
                }
            }),
            Ref.auto(hf.entity.Player.killHit).before(function(hf: hf.Hf, self: hf.entity.Player, dx: Null<Float>): Void {
                self.game.fxMan.attachAlert("GAME OVER");

                for (bad in self.game.getBadClearList()) {
                    untyped { bad.update = null; }
                }
                for (shoot in self.game.getList(hf.Data.SHOOT)) {
                    untyped { shoot.update = null; }
                }
            }),
            Ref.auto(hf.entity.Player.update).before(function(hf: hf.Hf, self: hf.entity.Player): Void {
                if (self.fl_kill) {
                    self.dy /= 2;
                    if (Std.random(3) >= 1) {
                        self.game.fxMan.attachExplodeZone(self.x  -30 + Std.random(60), self.y - self._height / 2 - 30 + Std.random(60), 15);
                    }
                }
            }),
            Ref.auto(hf.entity.shoot.Ball.update).after(function(hf: hf.Hf, self: hf.entity.shoot.Ball): Void {
                if ((cast self).balleBoss) {
                    if (self.lifeTimer <= 0) {
                        self.game.fxMan.attachFx(self.x, self.y - hf.Data.CASE_HEIGHT / 2, 'hammer_fx_pop');
                        self.destroy();
                        this.boss.balle = null;
                        this.boss.fl_ball = true;
                    }
                }
            }),
            Ref.auto(hf.entity.bad.FireBall.update).wrap(function(hf: hf.Hf, self: hf.entity.bad.FireBall, old): Void {
                if (!IS_BOSS_FRUIT.get(self)) {
                    old(self);
                    return;
                }

                if (self.fl_summon) {
                    self.summonTimer -= hf.Timer.tmod;
                    if (self.summonTimer <= 0) {
                        self.fl_summon = false;
                        self.stopBlink();
                    }
                }
                if (self.player.fl_kill || self.player._name == null) {
                    self.moveToAng(self.ang, self.speed * self.speedFactor);
                    Ref.call(hf, super.update, self);
                    return;
                }
                self.tang = Math.atan2(self.player.y - self.y, self.player.x - self.x);
                self.tang = self.adjustAngle(self.tang * 180 / Math.PI);
                if (self.ang - self.tang > 180) {
                    self.ang -= 360;
                }
                if (self.tang - self.ang > 180) {
                    self.ang += 360;
                }
                if (self.ang < self.tang) {
                    self.ang += self.angSpeed * self.speedFactor * hf.Timer.tmod;
                    if (self.ang > self.tang) {
                        self.ang = self.tang;
                    }
                }
                if (self.ang > self.tang) {
                    self.ang -= self.angSpeed * self.speedFactor * hf.Timer.tmod;
                    if (self.ang < self.tang) {
                        self.ang = self.tang;
                    }
                }
                self.ang = self.adjustAngle(self.ang);
                self.moveToAng(self.ang, self.speed * self.speedFactor);
                Ref.call(hf, super.update, self);
            }),
            Ref.auto(hf.entity.Bad.update).after(function(hf: hf.Hf, self: hf.entity.Bad): Void {
                if (!bossActive) {
                    return;
                }
                if (IS_BOSS_FRUIT.get(self)) {
                    if (this.boss.invincibilityTimer > 0) {
                        this.boss.invincibilityTimer--;
                    }
                    if (this.boss.citronAttackTimer > 0) {
                        this.boss.citronAttackTimer--;
                        if (this.boss.citronAttackTimer % Math.floor(self.game.root.Data.SECOND / 3) == 0) {
                            hf.entity.shoot.Zeste.attach(self.game, self.x, self.y - 30).moveUp(6);
                        }
                        if (this.boss.citronAttackTimer == 0) {
                            this.boss.fl_shooter = true;
                            this.boss.anger = 0;
                            this.boss.updateSpeed();
                        }
                    }
                    if (this.boss.bombinoExplosionTimer > 0) {
                        this.boss.bombinoExplosionTimer--;
                        if (this.boss.bombinoExplosionTimer == 0) {
                            this.boss.stopBlink();
                            this.boss.loseHP(self.game, 10);
                            self.game.fxMan.attachExplodeZone(self.x, self.y, self.game.root.Data.CASE_WIDTH * 7);
                            for (player in self.game.getPlayerList()) {
                                if (self.distance(player.x, player.y) <= self.game.root.Data.CASE_WIDTH * 7) {
                                    player.killHit(0);
                                }
                            }
                        }
                    }
                    if (this.boss.id == self.game.root.Data.BAD_FRAISE && !this.boss.fl_knock) {
                        this.boss.fraiseAttackTimer--;
                        if (this.boss.fraiseAttackTimer <= 0 && Std.random(10) <= 3) {
                            this.boss.fraiseAttackTimer = 160;
                            var balle = hf.entity.shoot.Ball.attach(self.game, self.x, self.y);
                            balle._xscale *= 2;
                            balle._yscale *= 2;
                            balle.moveToTarget(self.game.getOne(hf.Data.PLAYER), balle.shootSpeed);
                            (cast balle).balleBoss = true;
                            (cast balle).onLifeTimer = null;
                            balle.unregister(hf.Data.BALL);
                            this.boss.balle = balle;
                            this.boss.fl_ball = false;
                        }
                    }
                    if (this.boss.dyingAnimationTimer > 0) {
                        this.boss.dyingAnimationTimer--;
                        if (this.boss.dyingAnimationTimer > 10) {
                            switch(this.boss.dyingAnimationTimer % 12) {
                                case 0: self.game.fxMan.attachExplodeZone(this.boss.x + Std.random(20) - 10, this.boss.y + Std.random(20) - 10, 70 + Std.random(60));
                                case 4: self.game.world.view.attachSprite("$explosion", this.boss.x + Std.random(20) + 10, this.boss.y + Std.random(20) - 30, false);
                                case 8: var lightning = self.game.depthMan.attach('hammer_fx_strike', self.game.root.Data.FX);
                                    lightning._x = self.game.root.Data.GAME_WIDTH * 0.5; lightning._y = this.boss._y + Std.random(12) - 20;
                                case 10: self.game.world.view.attachSprite("$explosion", this.boss.x + Std.random(20) + 10, this.boss.y + Std.random(20) - 30, false);
                            }
                        }
                        else if (this.boss.dyingAnimationTimer == 0) {
                            this.bossActive = false;
                            this.boss.destroy();
                        }
                    } else {
                        ++this.fruitPipes.timer;
                        if (this.fruitPipes.timer % this.fruitPipes.interval == 0 && self.game.getBadClearList().length <= this.fruitPipes.max) {
                            this.spawnFruitInFruitPipe(self.game);
                        }
                    }
                    if (this.boss.balle != null) {
                        var centreBalle = {"x": this.boss.balle.x + this.boss.balle._width / 2, "y": this.boss.balle.y - this.boss.balle._height / 2};
                        for (lever in (this.levers.list : Array<Dynamic>)) {
                            if (lever.active == false && Math.sqrt(Math.pow(lever.y * 20 + 10 - centreBalle.y, 2) + Math.pow(lever.x * 20 + 10 - centreBalle.x, 2)) <= hf.Data.CASE_WIDTH) {
                                lever.active = true;
                                lever.sprite.removeMovieClip();
                                lever.sprite = self.game.world.view.attachSprite("$levier2", lever.x * 20, (lever.y + 1) * 20, true);
                            }
                        }
                        this.checkIfAllLeversActivated(self.game);
                    }
                    if (this.levers.timer > 0) {
                        this.levers.timer--;
                        if (this.levers.timer == 0) {
                            for (lever in (this.levers.list : Array<Dynamic>)) {
                                lever.active = false;
                                lever.sprite.removeMovieClip();
                                lever.sprite = self.game.world.view.attachSprite("$levier1", lever.x * 20, (lever.y + 1) * 20, true);
                            }
                        }
                    }
                    if (this.boss.id == self.game.root.Data.BAD_BALEINE) {
                        if (this.boss.y - this.boss._height <= 0) {
                            this.boss.dy = Math.abs(this.boss.dy);
                        }
                    }
                    else if (this.boss.id == self.game.root.Data.BAD_FRAMBOISE) {
                        if (this.boss.isReady() && self.game.getClose(self.game.root.Data.PLAYER, self.x, self.y, 90, false).length != 0) {
                            this.boss.startShoot();
                        }
                    }
                }
                else {
                    if (FRUIT_PIPE_SPAWN_TIMER.get(self) > 0 && FRUIT_PIPE_SPAWN_TIMER.get(self) <= self.game.root.Data.SECOND * 1) {
                        FRUIT_PIPE_SPAWN_TIMER.set(self, FRUIT_PIPE_SPAWN_TIMER.get(self) + 1);
                        switch (FRUIT_PIPE_DIR.get(self)) {
                            case 0: self.x++;
                            case 1: self.y--;
                            case 2: self.x--;
                            case 3: self.y++;
                        }
                        if (FRUIT_PIPE_SPAWN_TIMER.get(self) >= self.game.root.Data.SECOND * 1) {
                            self.fl_moveable = true;
                            self.fl_gravity = true;
                        }
                    }
                }
            })
        ];
        this.patches = FrozenArray.from(patches);
    }

    public function createBossBad(game: hf.mode.GameMode, x: Float, y: Float, i: Int, hp: Int) {
        this.bossActive = true;
        if (i == game.root.Data.BAD_FIREBALL) {
            this.boss = game.root.entity.bad.FireBall.attach(game, game.getPlayerList()[0]);
            this.boss.register(game.root.Data.BAD_CLEAR);
        }
        else {
            this.boss = game.attachBad(i, x * 20 + 10, y * 20);
        }
        this.boss.scale(200);
        this.boss.id = i;
        IS_BOSS_FRUIT.set(boss, true);
        this.boss.hp = hp;
        this.boss.fl_moveable = false;
        this.boss.eatsPlayerBombs = true;
        this.boss.canBeHitByBlackBomb = false;
        this.boss.canBeHitBySurfing = true;
        this.boss.canKillFruits = true;
        this.boss.dyingAnimationTimer = 0;
        this.boss.invincibilityTimer = 0;
        this.boss.bombinoExplosionTimer = 0;
        this.boss.citronAttackTimer = 0;
        this.boss.pommeAttackCount = 0;
        this.boss.fraiseAttackTimer = 0;
        this.boss.peutSauter3 = true;
        this.boss.peutMonter3 = true;
        this.boss.peutGrimper3 = true;

        this.boss.lifeBar = game.depthMan.attach("hammer_interf_boss_bar", game.root.Data.DP_INTERF);
        this.boss.lifeBar._rotation = -90;
        this.boss.lifeBar._x = 0;
        this.boss.lifeBar._y = game.root.Data.GAME_HEIGHT * 0.5;
        this.boss.lifeBarMaxHp = this.boss.hp;

        this.boss.knock = function() {};
        this.boss.killHit = function() {};
        this.boss.freeze = function() {};
        this.boss.forceKnock = function(t: Int) {
            if (this.boss.fl_knock) {
                return;
            }
            (cast this.boss).phaseIn();
            Ref.call(game.root, hf.entity.Bad.knock, this.boss, t);
        };
        this.boss.loseHP = function(game: hf.mode.GameMode, removedHP: Int) {
            if (this.boss.invincibilityTimer > 0) {
                return;
            }
            this.boss.invincibilityTimer = game.root.Data.SECOND * 1.7;
            this.boss.hp -= removedHP;
            if (this.boss.id == game.root.Data.BAD_CITRON) {
                this.boss.citronAttackTimer = 0;
                this.boss.fl_shooter = true;
                this.boss.anger = 0;
                this.boss.updateSpeed();
            }
            else if (this.boss.id == game.root.Data.BAD_ANANAS) {
                this.boss.fallFactor *= 1.5;
                this.boss.fl_attack = false;
                this.boss.unstick();
                var explosion = game.fxMan.attachExplodeZone(this.boss.x, this.boss.y, this.boss.dashRadius);
                explosion.mc._alpha = 10;
                for (player in game.getClose(game.root.Data.PLAYER, this.boss.x, this.boss.y, this.boss.dashRadius, false)) {
                    (cast player).changeWeapon(game.root.Data.WEAPON_B_CLASSIC);
                    game.fxMan.attachExplodeZone(player.x + player._width / 2, player.y - player._height / 2, 20);
                }
            }
            else if (this.boss.id == game.root.Data.BAD_FIREBALL) {
                this.boss.speedFactor += 0.1;
            }
            game.shake(game.root.Data.SECOND * 2, 3);
            if (this.boss.hp > 0) {
                this.boss.lifeBar.barFade._xscale = this.boss.lifeBar.bar._xscale;
                this.boss.lifeBar.barFade.gotoAndPlay("1");
                this.boss.lifeBar.bar._xscale = (this.boss.hp / this.boss.lifeBarMaxHp) * 100;
            }
            else {
                this.boss.lifeBar.removeMovieClip();
                this.boss.onShoot = null;
                this.boss.attack = null;
                if (this.boss.id == game.root.Data.BAD_LITCHI) {
                    var x = this.boss.x;
                    var y = this.boss.y;
                    this.boss.destroy();
                    this.createBossBad(game, game.root.Entity.x_rtc(x) - 0.5, game.root.Entity.y_rtc(y), game.root.Data.BAD_LITCHI_WEAK, 70);
                    game.fxMan.inGameParticles(game.root.Data.PARTICLE_LITCHI, x, y, 50);
                    return;
                }
                for (bad in game.getBadClearList()) {
                    if (bad != this.boss || this.boss.id == game.root.Data.BAD_BOMBE) {
                        bad.destroy();
                    }
                }
                if (this.boss.id == game.root.Data.BAD_FRAMBOISE) {
                    game.destroyList(game.root.Data.SHOOT);
                }
                this.boss.dyingAnimationTimer = 70;
                if (this.boss.id == game.root.Data.BAD_FRAMBOISE) {
                    this.boss.minAlpha = 0;
                    this.boss.alpha = 0;
                }
                else {
                    this.boss._visible = false;
                }

                this.boss.dx = 0;
                this.boss.dy = 0;
                this.boss.fl_physics = false;
                this.boss.fl_shooter = false;
            }
        };

        game.fl_clear = false;
        game.badCount++;

        this.fruitPipes = {};
        this.fruitPipes.timer = 0;
        this.fruitPipes.max = 0;
        this.fruitPipes.interval = 0;
        this.fruitPipes.allAtOnce = false;
        this.fruitPipes.list = [];

        this.levers = {};
        this.levers.list = [];
        this.levers.timer = 0;

        if (this.boss.id == game.root.Data.BAD_CERISE) {

        }
        else if (this.boss.id == game.root.Data.BAD_ORANGE) {

        }
        else if (this.boss.id == game.root.Data.BAD_POMME) {
            this.boss.setJumpDown(3);
        }
        else if (this.boss.id == game.root.Data.BAD_BANANE) {
            this.boss.eatsPlayerBombs = false;
            this.boss.canBeHitByBlackBomb = true;
            this.boss.canBeHitBySurfing = false;
        }
        else if (this.boss.id == game.root.Data.BAD_CITRON) {
            this.boss.eatsPlayerBombs = false;
            this.boss.canBeHitByBlackBomb = true;
            this.boss.canBeHitBySurfing = false;
            this.boss.canKillFruits = false;

        }
        else if (this.boss.id == game.root.Data.BAD_BOMBE) {
            this.boss.canKillFruits = false;
            this.boss.setJumpDown(null);
        }
        else if (this.boss.id == game.root.Data.BAD_POIRE) {

        }
        else if (this.boss.id == game.root.Data.BAD_ABRICOT) {
            this.boss.setJumpDown(null);
            this.boss.setFall(null);
        }
        else if (this.boss.id == game.root.Data.BAD_LITCHI) {
            this.boss.setJumpDown(null);
            this.boss.setFall(null);
            this.boss.setClimb(null, null);
        }
        else if (this.boss.id == game.root.Data.BAD_LITCHI_WEAK) {
            this.boss.setJumpDown(null);
            this.boss.setFall(null);
            this.boss.setClimb(null, null);
            this.boss.angerMore();
        }
        else if (this.boss.id == game.root.Data.BAD_FRAISE) {
            this.boss.onLifeTimer = null;
            this.boss.assignBall = null;
            this.boss.startShoot = null;
            this.boss.catchBall = null;
            this.boss.onStateEnd = null;
            this.boss.unregister(game.root.Data.CATCHER);
            this.boss.fraiseAttackTimer = 160;
        }
        else if (this.boss.id == game.root.Data.BAD_KIWI) {
            this.boss.canBeHitBySurfing = false;
        }
        else if (this.boss.id == game.root.Data.BAD_BALEINE) {
            this.boss.scale(150);
        }
        else if (this.boss.id == game.root.Data.BAD_ANANAS) {
            this.boss.eatsPlayerBombs = false;
            this.boss.canBeHitByBlackBomb = true;
            this.boss.canBeHitBySurfing = false;
            this.boss.dashRadius = 200;

        }
        else if (this.boss.id == game.root.Data.BAD_CRAWLER) {
            this.boss.scale(150);
        }
        else if (this.boss.id == game.root.Data.BAD_FRAMBOISE) {
            this.boss.setJumpDown(null);
            this.boss.setFall(null);
            this.boss.setClimb(null, null);
        }
        else if (this.boss.id == game.root.Data.BAD_FIREBALL) {

        }
    }

    public function initFruitPipes(game: hf.mode.GameMode, max: Int, t: Int, all_at_once: Bool) {
        this.fruitPipes.max = max;
        this.fruitPipes.interval = t;
        this.fruitPipes.allAtOnce = all_at_once;
    }

    public function addFruitPipe(game: hf.mode.GameMode, xr: Int, yr: Int, dir: Int) {
        this.fruitPipes.list.push({"x": xr, "y": yr, "dir": dir});
    }

    public function addBossLever(game: hf.mode.GameMode, x: Float, y: Float) {
        this.levers.list.push({"x": x, "y": y, "active": false, "sprite": game.world.view.attachSprite("$levier1", x * 20, (y + 1) * 20, true)});
    }

    public function checkIfAllLeversActivated(game: hf.mode.GameMode) {
        if (this.levers.timer > 0) {
            return;
        }

        var allActive = true;
        for (lever in (this.levers.list : Array<Dynamic>)) {
            if (lever.active == false) {
                allActive = false;
                break;
            }
        }
        if (allActive) {
            this.boss.forceKnock(1.7 * game.root.Data.SECOND);
            for (bad in game.getBadClearList()) {
                bad.knock(1.7 * game.root.Data.SECOND);
            }
            game.fxMan.attachExplodeZone(this.boss.x, this.boss.y, 40);
            for (i in 0...5) {
                var eclair = game.depthMan.attach('hammer_fx_strike', game.root.Data.FX);
                eclair._x = this.boss.x;
                eclair._y = this.boss.y;
                eclair._xscale *= Std.random(2) * 2 - 1;
                eclair._rotation = Std.random(180);
            }

            this.boss.loseHP(game, 10);
            this.levers.timer = game.root.Data.SECOND * 2;
        }
    }

    public function spawnFruitInFruitPipe(game: hf.mode.GameMode) {
        var pipes: Array<Dynamic> = [];
        pipes = pipes.concat(this.fruitPipes.allAtOnce ? this.fruitPipes.list : this.fruitPipes.list[Std.random(this.fruitPipes.list.length)]);
        for (pipe in pipes) {
            var fruit: Dynamic;
            if (this.boss.id == game.root.Data.BAD_FIREBALL) {
                if (game.getPlayerList().length == 0) {
                    return;
                }
                fruit = game.root.entity.bad.FireBall.attach(game, game.getPlayerList()[0]);
                fruit.register(game.root.Data.BAD_CLEAR);
            }
            else {
                fruit = game.attachBad(this.boss.id, pipe.x, pipe.y);
                fruit.knock(game.root.Data.SECOND * 3);
                fruit.fl_moveable = false;
                fruit.fl_gravity = false;
                FRUIT_PIPE_SPAWN_TIMER.set(fruit, 1);
                FRUIT_PIPE_DIR.set(fruit, pipe.dir);
            }
            IS_BOSS_FRUIT.set(fruit, false);

            if (this.boss.id == game.root.Data.BAD_BANANE) {
                fruit.peutMonter3 = true;
                fruit.peutSauter3 = true;
                fruit.angerMore();
            }
            else if (this.boss.id == game.root.Data.BAD_LITCHI_WEAK) {
                fruit.angerMore();
            }
        }
    }

    public function bossHPBelow(game: hf.mode.GameMode, hp: Int) {
        return this.bossActive && this.boss.hp <= hp;
    }

    public function checkBoss(game: hf.mode.GameMode, id: Int) {
        return this.bossActive && this.boss.id == id;
    }
}

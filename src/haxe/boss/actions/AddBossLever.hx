package boss.actions;

import color_matrix.ColorMatrix;
import etwin.flash.filters.ColorMatrixFilter;
import merlin.IAction;
import merlin.IActionContext;
import patchman.Assert;
import etwin.Obfu;

class AddBossLever implements IAction {
    public var name(default, null): String = Obfu.raw("add_boss_lever");
    public var isVerbose(default, null): Bool = false;

    private var mod: Boss;

    public function new(mod: Boss) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var x: Float = ctx.getFloat(Obfu.raw("x"));
        var y: Float = ctx.getFloat(Obfu.raw("y"));

        this.mod.addBossLever(game, x, y);

        return false;
    }
}

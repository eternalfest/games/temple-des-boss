package boss.actions;

import color_matrix.ColorMatrix;
import etwin.flash.filters.ColorMatrixFilter;
import merlin.IAction;
import merlin.IActionContext;
import patchman.Assert;
import etwin.Obfu;

class BossBad implements IAction {
    public var name(default, null): String = Obfu.raw("boss_bad");
    public var isVerbose(default, null): Bool = false;

    private var mod: Boss;

    public function new(mod: Boss) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var x: Float = ctx.getFloat(Obfu.raw("x"));
        var y: Float = ctx.getFloat(Obfu.raw("y"));
        var i: Int = ctx.getInt(Obfu.raw("i"));
        var hp: Int = ctx.getInt(Obfu.raw("hp"));

        this.mod.createBossBad(game, x, y, i, hp);

        return false;
    }
}

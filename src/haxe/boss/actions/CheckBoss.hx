package boss.actions;

import color_matrix.ColorMatrix;
import etwin.flash.filters.ColorMatrixFilter;
import merlin.IAction;
import merlin.IActionContext;
import patchman.Assert;
import etwin.Obfu;

class CheckBoss implements IAction {
    public var name(default, null): String = Obfu.raw("check_boss");
    public var isVerbose(default, null): Bool = false;

    private var mod: Boss;

    public function new(mod: Boss) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var id: Int = ctx.getInt(Obfu.raw("id"));

        return this.mod.checkBoss(game, id);
    }
}

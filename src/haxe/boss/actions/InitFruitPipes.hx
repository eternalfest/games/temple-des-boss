package boss.actions;

import color_matrix.ColorMatrix;
import etwin.flash.filters.ColorMatrixFilter;
import merlin.IAction;
import merlin.IActionContext;
import patchman.Assert;
import etwin.Obfu;

class InitFruitPipes implements IAction {
    public var name(default, null): String = Obfu.raw("init_fruit_pipes");
    public var isVerbose(default, null): Bool = false;

    private var mod: Boss;

    public function new(mod: Boss) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var max: Int = ctx.getInt(Obfu.raw("max"));
        var t: Int = ctx.getInt(Obfu.raw("t"));
        var all_at_once: Bool = ctx.getOptBool(Obfu.raw("all_at_once")).or(false);

        this.mod.initFruitPipes(game, max, t, all_at_once);

        return false;
    }
}

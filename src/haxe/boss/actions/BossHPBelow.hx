package boss.actions;

import color_matrix.ColorMatrix;
import etwin.flash.filters.ColorMatrixFilter;
import merlin.IAction;
import merlin.IActionContext;
import patchman.Assert;
import etwin.Obfu;

class BossHPBelow implements IAction {
    public var name(default, null): String = Obfu.raw("boss_hp_below");
    public var isVerbose(default, null): Bool = false;

    private var mod: Boss;

    public function new(mod: Boss) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var hp: Int = ctx.getInt(Obfu.raw("hp"));

        return this.mod.bossHPBelow(game, hp);
    }
}

package boss.actions;

import color_matrix.ColorMatrix;
import etwin.flash.filters.ColorMatrixFilter;
import merlin.IAction;
import merlin.IActionContext;
import patchman.Assert;
import etwin.Obfu;

class AddFruitPipe implements IAction {
    public var name(default, null): String = Obfu.raw("add_fruit_pipe");
    public var isVerbose(default, null): Bool = false;

    private var mod: Boss;

    public function new(mod: Boss) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var xr: Int = ctx.getInt(Obfu.raw("xr"));
        var yr: Int = ctx.getInt(Obfu.raw("yr"));
        var dir: Int = ctx.getInt(Obfu.raw("dir"));

        this.mod.addFruitPipe(game, xr, yr, dir);

        return false;
    }
}

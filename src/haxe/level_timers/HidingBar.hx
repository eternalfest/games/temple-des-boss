package level_timers;

import bottom_bar.modules.BottomBarModule;
import bottom_bar.BottomBarInterface;

class HidingBar extends BottomBarModule {
    public var level_timers: LevelTimers;
    public var hidingBar: Dynamic;

    public function new(module: Map<String, Dynamic>, level_timers: LevelTimers) {
        this.level_timers = level_timers;
    }

    public override function init(bottomBar: BottomBarInterface): Void {
        this.hidingBar = bottomBar.game.depthMan.empty(bottomBar.game.root.Data.DP_TOP);
        this.hidingBar.beginFill(7366029);
        this.hidingBar._alpha = 100;
        this.hidingBar.moveTo(-10, 500);
        this.hidingBar.lineTo(-10, 520);
        this.hidingBar.lineTo(420, 520);
        this.hidingBar.lineTo(420, 500);
        this.hidingBar.lineTo(-10, 500);
        this.hidingBar.endFill();
    }

    public override function update(bottomBar: BottomBarInterface): Bool {
        return true;
    }
}
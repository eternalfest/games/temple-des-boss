package level_timers;

import bottom_bar.modules.BottomBarModule;
import bottom_bar.BottomBarInterface;

import etwin.Obfu;
import etwin.ds.FrozenArray;
import patchman.IPatch;

class FruitSprites extends BottomBarModule {
    public var level_timers: LevelTimers;
    public var x: Int;
    public var fruitSprites: Array<Dynamic>;
    public var x_list: Array<Int>;

    public function new(module: Map<String, Dynamic>, level_timers: LevelTimers) {
        this.level_timers = level_timers;
        this.x = module[Obfu.raw("x")];
        this.fruitSprites = new Array<Dynamic>();
        this.level_timers.id_list = module[Obfu.raw("id_list")];
        this.x_list = module[Obfu.raw("x_list")];
        this.level_timers.scale_list = module[Obfu.raw("scale_list")];
    }

    public override function init(bottomBar: BottomBarInterface): Void {
        for (id_index in 0...this.level_timers.id_list.length) {
            this.fruitSprites[id_index] = bottomBar.game.depthMan.attach(bottomBar.game.root.Data.initLinkages()[this.level_timers.id_list[id_index]], bottomBar.game.root.Data.DP_TOP + 1);
            this.fruitSprites[id_index]._xscale = this.level_timers.scale_list[id_index];
            this.fruitSprites[id_index]._yscale = this.level_timers.scale_list[id_index];
            this.fruitSprites[id_index]._x = this.x + this.x_list[id_index];
            this.fruitSprites[id_index]._y = 518;
            this.fruitSprites[id_index].setColorHex(100, 0);
            this.fruitSprites[id_index].sub.stop();
        }
    }

    public override function update(bottomBar: BottomBarInterface): Bool {
        this.level_timers.fruitSprites = this.fruitSprites;
        return true;
    }
}
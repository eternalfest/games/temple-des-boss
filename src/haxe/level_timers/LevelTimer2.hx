package level_timers;

import bottom_bar.modules.custom.LevelTimer;
import bottom_bar.modules.BottomBarModule;
import bottom_bar.BottomBarInterface;

class LevelTimer2 extends LevelTimer {
    public var level_timers: LevelTimers;

    public function new(module: Dynamic, level_timers: LevelTimers) {
        super(module);
        this.level_timers = level_timers;
    }

    public override function update(bottomBar: BottomBarInterface): Bool {
        super.update(bottomBar);
        this.level_timers.elapsedTime = this.elapsedTime;
        return true;
    }
}
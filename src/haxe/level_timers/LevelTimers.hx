package level_timers;

import level_timers.actions.SaveTime;

import bottom_bar.modules.BottomBarModule;
import bottom_bar.BottomBarInterface;
import bottom_bar.BottomBar;

import merlin.IAction;
import patchman.IPatch;
import patchman.Ref;
import patchman.module.GameEvents;
import etwin.ds.FrozenArray;
import etwin.ds.WeakMap;
import etwin.Obfu;


@:build(patchman.Build.di())
class LevelTimers {
    @:diExport
    public var actions(default, null): FrozenArray<IAction>;
    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    public var elapsedTime: Int = 0;
    public var savedTimes = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    public var timerAnimationTimer: Int = -1;
    public var timerAnimationSprite: Dynamic;
    private var gameEvents: GameEvents;

    public var fruitSprites: Array<Dynamic>;
    public var pauseBackgroundSprite = null;
    public var id_list = [];
    public var scale_list = [];
    public var fruitPauseSprites = [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null];
    public var savedTimesPauseSprites = [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null];

    public function new(gameEvents: GameEvents) {
        this.gameEvents = gameEvents;

        this.actions = cast FrozenArray.of(
            new SaveTime(this)
        );

        var patches = [
            Ref.auto(hf.mode.GameMode.initInterface).after(function(hf: hf.Hf, self: hf.mode.GameMode): Void {
                this.pauseBackgroundSprite = self.depthMan.empty(hf.Data.DP_TOP);
                this.pauseBackgroundSprite.beginFill(6181190);
                this.pauseBackgroundSprite._alpha = 50;
                this.pauseBackgroundSprite.moveTo(-10,0);
                this.pauseBackgroundSprite.lineTo(-10,520);
                this.pauseBackgroundSprite.lineTo(420,520);
                this.pauseBackgroundSprite.lineTo(420,0);
                this.pauseBackgroundSprite.lineTo(-10,0);
                this.pauseBackgroundSprite.endFill();
                this.pauseBackgroundSprite._visible = false;


                for (i in 0...16) {
                    this.fruitPauseSprites[i] = self.depthMan.attach(hf.Data.initLinkages()[this.id_list[i]], hf.Data.DP_TOP);
                    this.fruitPauseSprites[i]._xscale = this.scale_list[i];
                    this.fruitPauseSprites[i]._yscale = this.scale_list[i];
                    this.fruitPauseSprites[i]._x = 130;
                    this.fruitPauseSprites[i]._y = 72 + i * 25;
                    (cast this.fruitPauseSprites[i]).setColorHex(100, 0);
                    (cast this.fruitPauseSprites[i]).mc.stop();
                    this.fruitPauseSprites[i]._visible = false;

                    this.savedTimesPauseSprites[i] = self.depthMan.attach("hammer_interf_inGameMsg", hf.Data.DP_TOP);
                    (cast this.savedTimesPauseSprites[i]).field._width = 400;
                    (cast this.savedTimesPauseSprites[i]).label.text = "";
                    (cast this.savedTimesPauseSprites[i]).field.text = "??:??:??.???";
                    (cast this.savedTimesPauseSprites[i]).field._xscale = 150;
                    (cast this.savedTimesPauseSprites[i]).field._yscale = 150;
                    (cast this.savedTimesPauseSprites[i]).field._x = 150;
                    (cast this.savedTimesPauseSprites[i]).field._y = 50 + i * 25;
                    self.root.FxManager.addGlow((cast this.savedTimesPauseSprites[i]), 0, 2);
                    (cast this.savedTimesPauseSprites[i])._visible = false;
                }
            }),
            Ref.auto(hf.mode.GameMode.onMap).wrap(function(hf: hf.Hf, self: hf.mode.GameMode, old): Void {
                self.fl_pause = true;
                self.lock();
                self.world.lock();
                if(!self.fl_mute) {
                    self.setMusicVolume(0.5);
                }
                this.pauseBackgroundSprite._visible = true;
                for (sprite in this.fruitPauseSprites) {
                    sprite._visible = true;
                }
                for (sprite in this.savedTimesPauseSprites) {
                    sprite._visible = true;
                }

            }),
            Ref.auto(hf.mode.GameMode.onUnpause).after(function(hf: hf.Hf, self: hf.mode.GameMode): Void {
                this.pauseBackgroundSprite._visible = false;
                for (sprite in this.fruitPauseSprites) {
                    sprite._visible = false;
                }
                for (sprite in this.savedTimesPauseSprites) {
                    sprite._visible = false;
                }
            }),
            Ref.auto(hf.mode.GameMode.main).after(function(hf: hf.Hf, self: hf.mode.GameMode): Void {
                if (self.fl_lock) {
                    return;
                }
                if (this.timerAnimationTimer >= 0) {
                    (cast this.timerAnimationSprite).field._x = (100 + Math.pow(((100 - this.timerAnimationTimer) - 50), 5) / 100000);
                    this.timerAnimationTimer--;
                }
            }),
            Ref.auto(hf.mode.Adventure.saveScore).replace(function(hf: hf.Hf, self: hf.mode.Adventure): Void {
                var maxLevel: Int = self.dimensions[0].currentId;
                var itemsTaken: Array<Int> = self.getPicks2();
                var items: Map<String, Int> = new Map();
                for (i in 0...itemsTaken.length) {
                    if (itemsTaken[i] != null) {
                        items.set(Std.string(i), itemsTaken[i]);
                    }
                }
                this.gameEvents.endGame(new ef.api.run.SetRunResultOptions(maxLevel == 1, maxLevel, self.savedScores, items, {
                    var obj = {};
                    Obfu.setField(obj, "savedTimes", this.savedTimes);
                    obj;
                }));
            })
        ];
        this.patches = FrozenArray.from(patches);
    }

    public function saveTime(game: hf.mode.GameMode, n: Int) {
        var timeInFrames: Int = this.elapsedTime;
        var formattedTime = this.formatTime(timeInFrames);

        this.savedTimes[n] = timeInFrames;
        this.fruitSprites[n].resetColor();
        (cast this.fruitPauseSprites[n]).resetColor();
        (cast this.savedTimesPauseSprites[n]).field.text = formattedTime;

        this.timerAnimationTimer = 100;
        this.createTimerAnimationSprite(game, formattedTime);
        return false;
    }
    public function createTimerPauseSprite(game: hf.mode.GameMode, n: Int, text: String) {

    }
    public function createTimerAnimationSprite(game: hf.mode.GameMode, text: String) {
        this.timerAnimationSprite.destroy();
        this.timerAnimationSprite = cast game.depthMan.attach('hammer_interf_inGameMsg', game.root.Data.DP_BORDERS);

        (cast this.timerAnimationSprite).label.text = "";
        (cast this.timerAnimationSprite).field._width = 400;
        (cast this.timerAnimationSprite).field.text = text;
        (cast this.timerAnimationSprite).field._xscale = 200;
        (cast this.timerAnimationSprite).field._yscale = 200;
        (cast this.timerAnimationSprite).field._x = -1000;
        (cast this.timerAnimationSprite).field._y = 230;
        (cast this.timerAnimationSprite).field._visible = true;

        game.root.FxManager.addGlow((cast this.timerAnimationSprite), 0, 2);
    }

    public function formatTime(timeInFrames: Int): String {
        var totalSeconds: Int = Std.int(timeInFrames / 40);
        var thousandths: Int = 25 * (timeInFrames % 40);
        var seconds: Int = totalSeconds % 60;
        var minutes: Int = cast (((totalSeconds - seconds) % 3600) / 60);
        var hours: Int = cast ((totalSeconds - minutes * 60 - seconds) / 3600);

        var text: String = "";
        if (hours < 10)
            text += "0";
        text += hours + ":";
        if (minutes < 10)
            text += "0";
        text += minutes + ":";
        if (seconds < 10)
            text += "0";
        text += seconds + ".";

        if (thousandths < 100)
            text += "0";
        if (thousandths < 10)
            text += "0";
        text += "" + thousandths;

        return text;
    }
}
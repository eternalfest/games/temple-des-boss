package level_timers.actions;

import color_matrix.ColorMatrix;
import etwin.flash.filters.ColorMatrixFilter;
import merlin.IAction;
import merlin.IActionContext;
import patchman.Assert;
import etwin.Obfu;

class SaveTime implements IAction {
    public var name(default, null): String = Obfu.raw("save_time");
    public var isVerbose(default, null): Bool = false;

    private var mod: LevelTimers;

    public function new(mod: LevelTimers) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var n: Int = ctx.getInt(Obfu.raw("n"));

        this.mod.saveTime(game, n);

        return false;
    }
}

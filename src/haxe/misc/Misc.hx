package misc;

import misc.actions.Geyser;
import misc.actions.Below;
import misc.actions.PipeWarp;
import misc.actions.ShakeScreen;
import misc.actions.CreateFallingStone;
import misc.actions.ActivateFallingStone;
import misc.actions.SetBlackBombDuration;
import misc.SetFlag;

import boss.Boss;

import merlin.IAction;
import patchman.IPatch;
import patchman.Ref;
import etwin.ds.FrozenArray;
import etwin.ds.WeakMap;


@:build(patchman.Build.di())
class Misc {
    @:diExport
    public var actions(default, null): FrozenArray<IAction>;
    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    private var bossModule: Boss;

    public static var PIPE_WARP_ACTIVE(default, never): WeakMap<hf.entity.Player, Bool> = new WeakMap();
    public static var PIPE_WARP_TIMER(default, never): WeakMap<hf.entity.Player, Int> = new WeakMap();
    public var fallingStoneSprites = [];
    public var fallingStoneInitX: Float = 0;
    public var fallingStoneInitY: Float = 0;
    public var fallingStoneSpeed: Float = 0;
    public var fallingStoneActive: Bool = false;
    public var blackBombDuration: Int = 100;

    public function new(bossModule: Boss): Void {
        this.bossModule = bossModule;

        this.actions = FrozenArray.of(
            new Geyser(this),
            new Below(this),
            new PipeWarp(this),
            new ShakeScreen(this),
            new CreateFallingStone(this),
            new ActivateFallingStone(this),
            new SetBlackBombDuration(this),
            new SetFlag()
        );

        var patches = [
            Ref.auto(hf.mode.Adventure.onLevelClear).before(function(hf: hf.Hf, self: hf.mode.Adventure): Void {
                self.perfectOrder = null;
            }),
            Ref.auto(hf.entity.Player.update).after(function(hf: hf.Hf, self: hf.entity.Player): Void {
                if (PIPE_WARP_ACTIVE.get(self)) {
                    self.y += 1;
                    PIPE_WARP_TIMER.set(self, PIPE_WARP_TIMER.get(self) + 1);
                    if (PIPE_WARP_TIMER.get(self) > self.game.root.Data.SECOND * 2) {
                        this.endPipeWarp(self.game);
                    }
                }
            }),
            Ref.auto(hf.entity.Player.killHit).wrap(function(hf: hf.Hf, self: hf.entity.Player, dx: Null<Float>, old): Void {
                if (PIPE_WARP_ACTIVE.get(self)) {
                    return;
                }
                old(self, dx);
            }),
            Ref.auto(hf.mode.GameMode.main).after(function(hf: hf.Hf, self: hf.mode.GameMode): Void {
                if (this.fallingStoneActive && !self.fl_lock) {
                    for (sprites in this.fallingStoneSprites) {
                        sprites._y += this.fallingStoneSpeed;
                    }
                    if (this.fallingStoneSprites[0]._y > self.root.Data.GAME_HEIGHT + 20) {
                        this.initFallingStone(self);
                        var smoke = self.fxMan.attachFx(this.fallingStoneSprites[0]._x + 10, this.fallingStoneSprites[0]._y + 10, "hammer_fx_pop");
                        untyped { smoke.scale(400); }
                    }
                    this.fallingStoneSpeed *= 1.1;

                    var leftSpike = this.fallingStoneSprites[1];
                    var rightSpike = this.fallingStoneSprites[2];
                    for (bad in self.getBadClearList()) {
                        if (!bad.fl_kill) {
                            if ((Math.abs(leftSpike._x - bad.x) < leftSpike._width && Math.abs(leftSpike._y - bad.y) < leftSpike._height * 1.5) ||
                                (Math.abs(rightSpike._x - bad.x) < rightSpike._width && Math.abs(rightSpike._y - bad.y) < rightSpike._height * 1.5)) {
                                bad.killHit(0);
                                self.fxMan.attachExplodeZone(bad.x - 5 + Std.random(10), bad.y - 5 + Std.random(10), 10 + Std.random(10));
                            }
                        }
                    }
                    for (player in self.getPlayerList()) {
                        if (!player.fl_kill) {
                            if ((Math.abs(leftSpike._x - player.x) < leftSpike._width && Math.abs(leftSpike._y - player.y) < leftSpike._height * 1.5) ||
                                (Math.abs(rightSpike._x - player.x) < rightSpike._width && Math.abs(rightSpike._y - player.y) < rightSpike._height * 1.5)) {
                                player.killHit(0);
                                self.fxMan.attachExplodeZone(player.x - 5 + Std.random(10), player.y - 5 + Std.random(10), 10 + Std.random(10));
                            }
                        }
                    }
                    if (this.bossModule.boss.invincibilityTimer <= 0) {
                        if ((Math.abs(leftSpike._x - this.bossModule.boss.x) < (this.bossModule.boss._width + leftSpike._width) / 2 && Math.abs(leftSpike._y - this.bossModule.boss.y) < (this.bossModule.boss._height + leftSpike._height) / 2) ||
                            (Math.abs(rightSpike._x - this.bossModule.boss.x) < (this.bossModule.boss._width + rightSpike._width) / 2 && Math.abs(rightSpike._y - this.bossModule.boss.y) < (this.bossModule.boss._height + rightSpike._height) / 2)) {
                            this.bossModule.boss.loseHP(self, 10);
                            self.fxMan.attachExplodeZone(this.bossModule.boss.x - 20 + Std.random(40), this.bossModule.boss.y - 20 + Std.random(40), 20 + Std.random(20));
                            self.fxMan.attachExplodeZone(this.bossModule.boss.x - 20 + Std.random(40), this.bossModule.boss.y - 20 + Std.random(40), 20 + Std.random(20));
                            self.fxMan.attachExplodeZone(this.bossModule.boss.x - 20 + Std.random(40), this.bossModule.boss.y - 20 + Std.random(40), 20 + Std.random(20));
                        }
                    }
                }
            }),

            Ref.auto(hf.entity.Bomb.initBomb).before(function(hf: hf.Hf, self: hf.entity.Bomb, g: hf.mode.GameMode, x: Float, y: Float): Void {
                if (Std.is(self, hf.entity.bomb.player.Black._class())) {
                    self.duration = this.blackBombDuration;
                }
            }),
        ];
        this.patches = FrozenArray.from(patches);

    }

    public function geyser(game: hf.mode.GameMode, x: Float) {
        game.fxMan.attachFx(x * 20, 380, "hammer_fx_death_player");
        for (bad in game.getBadList()) {
            if (Math.abs(bad.x - x * 20) <= 30 && bad.y >= 200) {
                bad.moveUp(20);
                bad.dx = 0;
                bad.dy = bad.fl_freeze ? -70 : -30;
            }
        }
    }

    public function below(game: hf.mode.GameMode, yr: Int) {
        if (game.world.scriptEngine.cycle > 32) {
            for (player in game.getPlayerList()) {
                return player.y >= yr && !player.fl_kill;
            }
        }
        return false;
    }

    public function startPipeWarp(game: hf.mode.GameMode) {
        var player = game.getPlayerList()[0];
        if (!PIPE_WARP_ACTIVE.get(player)) {
            player.fl_physics = false;
            player.fl_gravity = false;
            player.lockControls(game.root.Data.SECOND * 2);
            player.dx = 0;
            PIPE_WARP_ACTIVE.set(player, true);
            PIPE_WARP_TIMER.set(player, 0);
        }
    }

    public function endPipeWarp(game: hf.mode.GameMode) {
        var player = game.getPlayerList()[0];
        player.fl_physics = true;
        player.fl_gravity = true;
        PIPE_WARP_ACTIVE.set(player, false);

        var dest_cx = Math.random() > 0.5 ? 1.5 : 18;
        var dest_cy = 11.9;
        player.x = dest_cx * 20 + 10;
        player.y = dest_cy * 20 + 10;
        player.knock(game.root.Data.SECOND * 1);
        game.fxMan.attachExplodeZone(player.x, player.y, 30);
    }

    public function shakeScreen(game: hf.mode.GameMode, t: Int, i: Int) {
        game.shake(t, i);
    }

    public function createFallingStone(game: hf.mode.GameMode, x: Float, y: Float) {
        this.fallingStoneInitX = x * 20 + 10;
        this.fallingStoneInitY = y * 20 + 20;

        var blockSprite = game.world.view.attachSprite("$block", 100, 100, true);
        var leftSpikeSprite = game.world.view.attachSprite("hammer_bad_spear", 100, 100, true);
        leftSpikeSprite.stop();
        untyped { leftSpikeSprite.sub.stop(); }
        leftSpikeSprite._rotation = 180;
        var rightSpikeSprite = game.world.view.attachSprite("hammer_bad_spear", 100, 100, true);
        rightSpikeSprite.stop();
        untyped { rightSpikeSprite.sub.stop(); }
        rightSpikeSprite._rotation = 180;

        this.fallingStoneSprites.push(blockSprite);
        this.fallingStoneSprites.push(leftSpikeSprite);
        this.fallingStoneSprites.push(rightSpikeSprite);

        this.initFallingStone(game);
    }

    public function initFallingStone(game: hf.mode.GameMode) {
        this.fallingStoneActive = false;
        this.fallingStoneSprites[0]._x = this.fallingStoneInitX;
        this.fallingStoneSprites[0]._y = this.fallingStoneInitY;
        this.fallingStoneSprites[1]._x = this.fallingStoneInitX + 10;
        this.fallingStoneSprites[1]._y = this.fallingStoneInitY + 20;
        this.fallingStoneSprites[2]._x = this.fallingStoneInitX + 30;
        this.fallingStoneSprites[2]._y = this.fallingStoneInitY + 20;
        this.fallingStoneSpeed = 1;
    }

    public function activateFallingStone(game: hf.mode.GameMode) {
        this.fallingStoneActive = true;
    }

    public function setBlackBombDuration(game: hf.mode.GameMode, t: Int) {
        this.blackBombDuration = t;
    }
}

package misc.actions;

import color_matrix.ColorMatrix;
import etwin.flash.filters.ColorMatrixFilter;
import merlin.IAction;
import merlin.IActionContext;
import patchman.Assert;
import etwin.Obfu;

class Geyser implements IAction {
    public var name(default, null): String = Obfu.raw("geyser");
    public var isVerbose(default, null): Bool = false;

    private var mod: Misc;

    public function new(mod: Misc) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var x: Float = ctx.getFloat(Obfu.raw("x"));

        this.mod.geyser(game, x);

        return false;
    }
}

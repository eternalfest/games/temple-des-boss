package misc.actions;

import color_matrix.ColorMatrix;
import etwin.flash.filters.ColorMatrixFilter;
import merlin.IAction;
import merlin.IActionContext;
import patchman.Assert;
import etwin.Obfu;

class SetBlackBombDuration implements IAction {
    public var name(default, null): String = Obfu.raw("set_black_bomb_duration");
    public var isVerbose(default, null): Bool = false;

    private var mod: Misc;

    public function new(mod: Misc) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var t: Int = ctx.getInt(Obfu.raw("t"));

        this.mod.setBlackBombDuration(game, t);

        return false;
    }
}

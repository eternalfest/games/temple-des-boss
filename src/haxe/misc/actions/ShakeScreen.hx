package misc.actions;

import color_matrix.ColorMatrix;
import etwin.flash.filters.ColorMatrixFilter;
import merlin.IAction;
import merlin.IActionContext;
import patchman.Assert;
import etwin.Obfu;

class ShakeScreen implements IAction {
    public var name(default, null): String = Obfu.raw("shake_screen");
    public var isVerbose(default, null): Bool = false;

    private var mod: Misc;

    public function new(mod: Misc) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var t: Int = ctx.getInt(Obfu.raw("t"));
        var i: Int = ctx.getInt(Obfu.raw("i"));

        this.mod.shakeScreen(game, t, i);

        return false;
    }
}

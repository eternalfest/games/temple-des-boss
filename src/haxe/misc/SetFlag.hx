package misc;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

class SetFlag implements IAction {
    public var name(default, null): String = Obfu.raw("setFlag");
    public var isVerbose(default, null): Bool = false;

    public function new() {}

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();

        var x = ctx.getInt(Obfu.raw("x"));
        var y = ctx.getInt(Obfu.raw("y"));
        var add = ctx.getOptBool(Obfu.raw("add")).toNullable();
        var rem = ctx.getOptBool(Obfu.raw("rem")).toNullable();
        var value = ctx.getInt(Obfu.raw("value"));

        if (add == null)
            add = true;
        if (rem != null)
            add = false;
        game.world.forceFlag({x: x, y: y}, 1 << value, add);

        return false;
    }
}

package pretty_portals.actions;

import color_matrix.ColorMatrix;
import etwin.flash.filters.ColorMatrixFilter;
import merlin.IAction;
import merlin.IActionContext;
import patchman.Assert;
import etwin.Obfu;

class OpenPortalHSV implements IAction {
    public var name(default, null): String = Obfu.raw("openPortalHSV");
    public var isVerbose(default, null): Bool = false;

    public function new() {}

    public function run(ctx: IActionContext): Bool {
        var x: Float = ctx.getFloat(Obfu.raw("x"));
        var y: Float = ctx.getFloat(Obfu.raw("y"));
        var pid: Int = ctx.getInt(Obfu.raw("pid"));
        var h: Float = ctx.getOptFloat(Obfu.raw("h")).toNullable();
        var s: Float = ctx.getOptFloat(Obfu.raw("s")).toNullable();
        var v: Float = ctx.getOptFloat(Obfu.raw("v")).toNullable();
        var custom: Bool = ctx.getOptBool(Obfu.raw("custom")).or(false);

        var game: hf.mode.GameMode = ctx.getGame();

        game.openPortal(x, y, pid);
        var p: hf.mode.GameMode.PortalMc = game.portalMcList[pid];
        Assert.debug(p != null);

        if (h == null) { h = 0; }
        if (s == null) { s = 1; }
        if (v == null) { v = 1; }

        var filter: ColorMatrixFilter = custom ? this.customTransform().toFilter() : ColorMatrix.fromHsv(h, s, v).toFilter();
        p.mc.filters = [filter];

        return false;
    }

    public function customTransform() {
        var _loc2_ = new Array();
        _loc2_ = _loc2_.concat([-369 / 566,407 / 1132,0,0,250245 / 1132]);
        _loc2_ = _loc2_.concat([-1873 / 849,1075 / 1698,0,0,105331 / 566]);
        _loc2_ = _loc2_.concat([0,0,0,0,0]);
        _loc2_ = _loc2_.concat([0,0,0,1,0]);
        return ColorMatrix.of(_loc2_);
    }
}

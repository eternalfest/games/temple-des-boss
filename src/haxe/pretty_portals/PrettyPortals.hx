package pretty_portals;

import pretty_portals.actions.OpenPortalHSV;

import merlin.IAction;
import patchman.IPatch;
import patchman.Ref;


@:build(patchman.Build.di())
class PrettyPortals {
    @:diExport
    public var openPortalHSV(default, null): IAction;

    public function new(): Void {
        this.openPortalHSV = new OpenPortalHSV();
    }
}

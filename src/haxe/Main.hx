import etwin.Obfu;
import bugfix.Bugfix;
import debug.Debug;
import hf.Hf;
import merlin.Merlin;
import game_params.GameParams;
import patchman.IPatch;
import patchman.Patchman;
import better_script.BetterScript;
import zones.Zones;
import atlas.Atlas;
import pretty_portals.PrettyPortals;
import maxmods.AddLink;
import misc.Misc;
import boss.Boss;
import quests.QuestManager;
import user_data.UserData;
import bottom_bar.BottomBar;
import bottom_bar.modules.BottomBarModuleFactory;
import level_timers.LevelTimers;
import level_timers.HidingBar;
import level_timers.LevelTimer2;
import level_timers.FruitSprites;
import custom_ai.CustomAI;


@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
      bugfix: Bugfix,
      debug: debug.Debug,
      gameParams: GameParams,
      betterScript: BetterScript,
      pretty_portals: PrettyPortals,
      bottom_bar: BottomBar,
      custom_ai: CustomAI,
      level_timers: LevelTimers,
      misc: Misc,
      boss: Boss,
      quests: QuestManager,
      userData: UserData,
      add_link: AddLink,
      zones: Zones,
      atlasNoFireball: atlas.props.NoFireball,
      atlas: atlas.Atlas,
      merlin: Merlin,
      patches: Array<IPatch>,
      hf: Hf
  ) {
      BottomBarModuleFactory.get().addModule(Obfu.raw("HidingBar"), function(data: Dynamic) return new HidingBar(data, level_timers));
      BottomBarModuleFactory.get().addModule(Obfu.raw("LevelTimer2"), function(data: Dynamic) return new LevelTimer2(data, level_timers));
      BottomBarModuleFactory.get().addModule(Obfu.raw("FruitSprites"), function(data: Dynamic) return new FruitSprites(data, level_timers));
    Patchman.patchAll(patches, hf);
  }
}
